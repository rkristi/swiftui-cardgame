//
//  war_challengeApp.swift
//  war-challenge
//
//  Created by Raios Kristi on 26.07.2021.
//

import SwiftUI

@main
struct war_challengeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
